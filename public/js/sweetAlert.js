

window.onload = function () {
    
    document.querySelector('.warning.confirm button').onclick = function () {                         
        swal({
            title: '¿Esta seguro de cerrar sesión?',
            text: 'Esta a punto de abandonar la APP',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar'
        }).then(function (result) {
            if (result.value) {
                document.location = '#/logeo'
            }
        });
    };

};


