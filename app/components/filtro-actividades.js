import Component from '@ember/component';
import $ from 'jquery';

export default Component.extend({
    
    didInsertElement() {

    this._super(...arguments);

        //creamos dos arreglos uno de los meses del año y otro para los dias de la semana

        var meses = new Array("ene.", "feb.", "mar.", "abr.", "may.", "jun.", "jul.", "ago.", "sep.", "oct.", "nov.", "dic.");
        var diasSemana = new Array("dom.", "lun.", "mar.", "mier.", "jue.", "vie.", "sáb.");

        //obtenemos la fecha de nuestras actividades
        var fecha = $("#fecha").text();

        //Obtenemos la fecha nueva de acuerdo a la fecha obtenida de las actividades
        var convertFecha = new Date(fecha);

        //Convertimos a texto la fecha obtenida gracias a los arreglos creados acteriormente
        var FechaInicio = diasSemana[convertFecha.getDay()] + ' ' + convertFecha.getUTCDate() + ' de ' + meses[convertFecha.getUTCMonth()] + ' de ' + convertFecha.getFullYear();
                
        //Retornamos la nueva fecha en texto y la asignamos para mostrarla en la vista
        return $("#newFecha").text(FechaInicio);

        //cada vez que la pagina recarge, nos mandara al inicio de la ruta
        var posicion = $("#up").offset().top;
            $("body").animate({ scrollTop: posicion}, 0);  
                           
    },
 

    actions: {

        //Creamos una accion y enviamos datos al controller en este caso es logged/actividades
        //para hacer transitionToRoute al detalle de la actividad seleccionada desde aqui 
        detalle(actividad) {

            this.sendAction('detail', actividad)

        },


        //Creamos una accion y enviamos datos al controller
        //para hacer transitionToRoute a si misma y hacer el filtro de actividades                
        filtrar(vehicle) {               

            this.sendAction('valor', vehicle)                                    
            
        },


    }


});
