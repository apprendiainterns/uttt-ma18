import Component from '@ember/component';
import { inject as service } from '@ember/service';
import FindQuery from 'ember-emberfire-find-query/mixins/find-query';
import $ from 'jquery';


export default Component.extend(FindQuery, {
    store: Ember.inject.service(),
    session: service(),
    firebaseApp: service(),
    
        didInsertElement() {

        Ember.run.scheduleOnce('afterRender', this, function () {            
               
            var idContacto = $("#contacto").text();
            var idEvento = $("#evento").text();
            var msjNuevos = 0;

            let uid = this.get('session.currentUser.uid')

            //Obtengo el id de la cuenta del usuario autenticado por medio de uid
            this.store.query('account', {

                orderBy: "uid",
                equalTo: uid

            }).then((cuentaOrigen) => {

                cuentaOrigen.forEach(element => {

                    //obtengo todos los chats de mi usuario autenticado
                    element.get("chat").then((myChats) => {

                        if (myChats.firstObject) {

                            myChats.forEach(elementChats => {


                                let idChat = elementChats.id

                                //consultar a mi contacto
                                this.store.find('account', idContacto).then(contact => {

                                    //obtengo los chats de mi contacto
                                    contact.get('chat').then(chatsContacto => {

                                        if (chatsContacto.firstObject) {

                                            chatsContacto.forEach(element => {
                                                let idChatContacto = element.id

                                                //comparo los chats en comun entre los dos usuarios
                                                if (idChat == idChatContacto) {

                                                    //Buscar todos los chats de la base de datos por evento
                                                    this.store.query('chat', {
                                                        orderBy: "event",
                                                        equalTo: idEvento

                                                    }).then((chatsEvento) => {

                                                        if (chatsEvento.firstObject) {
                                                            chatsEvento.forEach(element => {
                                                                let idChatsEvento = element.id;
                                                                //comparo los chats en comun de los usuarios con los del evento
                                                                if (idChat == idChatsEvento) {


                                                                    //Obtengo todos los mensajes con id del chat
                                                                    this.store.query("message", {

                                                                        orderBy: "chat",
                                                                        equalTo: idChatsEvento

                                                                    }).then((chatUno) => {

                                                                        chatUno.forEach(uno => {
                                                                            //Obtengo todos los mensajes con id del chat
                                                                            this.store.query("message", {

                                                                                orderBy: "sender",
                                                                                equalTo: idContacto

                                                                            }).then((chatDos) => {

                                                                                if (chatDos.firstObject) {
                                                                                    chatDos.forEach(dos => {

                                                                                        //obtener los mensajes que envio el contacto que pertenecen al chat y comparar
                                                                                        if (uno.id == dos.id) {

                                                                                            this.store.query('message', {
                                                                                                orderBy: "estado",
                                                                                                equalTo: "enviado"
                                                                                            }).then((contarMensajes) => {

                                                                                                contarMensajes.forEach(msjNoLeido => {

                                                                                                    //comparar los mensajes que envio el contacto para obtener el numero de
                                                                                                    //mensajes recibidos

                                                                                                    if (uno.id == msjNoLeido.id) {

                                                                                                        msjNuevos++;                                                                                                        

                                                                                                        $("#text").text("mensajes");
                                                                                                        $("#recibidos").text(msjNuevos);
                                                                                                        
                                                                                                    } else {
                                                                                                        $("#recibidos").text("0");
                                                                                                    }
                                                                                                })
                                                                                            })
                                                                                        } else {
                                                                                            $("#recibidos").text("0");
                                                                                        }

                                                                                    })
                                                                                } else {
                                                                                    $("#recibidos").text("0");
                                                                                }
                                                                            })
                                                                        })
                                                                    })
                                                                } else {
                                                                    $("#recibidos").text("0");
                                                                }
                                                            })
                                                        } else {
                                                            $("#recibidos").text("0");
                                                        }
                                                    })
                                                } else {
                                                    $("#recibidos").text("0");
                                                }
                                            })
                                        } else {
                                            $("#recibidos").text("0");
                                        }
                                    })
                                })
                            })
                        } else {
                            $("#recibidos").text("0");
                        }
                    })
                })
            })
        })
    },
});
