import Controller from '@ember/controller';
import { validator, buildValidations } from 'ember-cp-validations';
import { inject as service } from '@ember/service';


const Validations = buildValidations({
    'token': [
        validator('presence', {
            presence: true,
            message: 'El campo no puede estar vacio'
        }),
    ]
});


export default Controller.extend(Validations, {
    firebaseApp: service(),

    actions: {
        logout() {
            swal({
                title: '¿Esta seguro de cerrar sesión?',
                text: 'Esta a punto de abandonar la APP',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {

                if (result.value) {
                    this.get('session').close().then(() => {
                    this.transitionToRoute('login')
                    })
                }
            });
        },

        toogleError(attr) {
            switch (attr) {
                case 'token':
                    this.set('tokenErrorCheck', true)
                    break
            }
        },

        newEvento(token) {
        },

        Ajustes() {

            let uid = this.get('session.currentUser.uid')  


            this.store.query('account', {

                orderBy: "uid",
                equalTo: uid

            }).then((cuentaOrigen) => {

                cuentaOrigen.forEach(element => {

                    //obtengo el id de mi usuario autenticado
                    let idCuentaUser = element.id;
                    console.log("4")
                    this.transitionToRoute('logged.ajustescuenta', idCuentaUser)

                })
            })
        
        },

        aboutEvento(){
            this.transitionToRoute ('logged.detail-evento')                        
        }

    }
});
