import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { validator, buildValidations } from 'ember-cp-validations';
import logged from '../../routes/logged';

/* Validaciones... */
const Validations = buildValidations({


    'interes': [
        validator('presence', {
            presence: true,
            message: 'El campo no puede estar vacio'
        }),
        validator('length', {
            max: 40,
            message: 'No debe de excederce a mas de 40 caracteres'
        })
    ],


});


export default Controller.extend(Validations, {
    firebaseApp: service(),

    actions: {

        toogleError(attr) {
            switch (attr) {
                case 'interes':
                    this.set('interesErrorCheck', true)
                    break
            }
        },

        actualizarPerfil(perfil) {

            perfil.save()

        },

        NuevaImg(img) {

            if (img) {

                var TmpPath = URL.createObjectURL(window.$('#client-image')[0].files[0]);
                window.$('#imgMOdal').attr("src", `${TmpPath}`)

                $("#error").text(img)
                $("#error").css({ color: "black" })
                $("#error").css({ width: "90%" })

            } else {

                $("#error").text("Debes Seleccionar una imagen")
                $("#error").css({ color: "black" })

            }
        },

        editarInteres() {

            if ($('#interes').css('display') != 'inline') {
                $('#interes').css({ 'display': 'inline' })
                $('#btnEditar').css({ 'display': 'none' })
            }
        },

        cancelar() {

            $('#btnEditar').css({ 'display': 'inline' })
            $('#interes').css({ 'display': 'none' })
            $('#textInteres').val("");

        },

        guardar(texto) {

            let uid = this.get('session.currentUser.uid')

            // Buscar la cuentadel usuario que esta autenticado
            this.store.query('account', {
                orderBy: 'uid',
                equalTo: uid
            }).then((user) => {

                user.forEach((datoUser) => {

                    datoUser.set('interes', texto);

                    datoUser.save().then(() => {

                        swal(
                            'Felicidades!',
                            'Interes cambiado con exito!',
                            'success'
                        ).then((result) => {

                            if (result.value) {
                                $('#btnEditar').css({ 'display': 'inline' })
                                $('#interes').css({ 'display': 'none' })
                                $('#textInteres').val("");
                            }
                        })
                    })
                })
            })
        },

        actualizar(IdUsu) {

            let nameImageUser;
            let image = document.getElementById('client-image');
            let uid = this.get('session.currentUser.uid')
            let file = image.files[0];
        
            if (file) {

                let metadata = {
                    'contentType': file.type
                };


                // Buscar la cuentadel usuario que esta autenticado
                this.store.query('account', {
                    orderBy: 'uid',
                    equalTo: uid
                }).then((r) => {

                    r.forEach((elem) => {
                        nameImageUser = elem.nombreImg
                    })

                    //Validaciones para hacer el cambio de imagen de acuerdo a la situacion

                    if (nameImageUser === "default") {

                        //Si el nombre de la imagen esta guardada como default

                        //Crea una ruta o referencia para guardar la imagen en el storage de firebase
                        let newRuta = this.get("firebaseApp").storage().ref().child(`imgUsers/${file.name}`).put(file, metadata);
                        newRuta.on('state_changed', null, (error) => {
                            console.error('Upload Failed:', error);
                        }, () => {

                            //En la variable newUrl se guarda la url de descarga de la imagen de firebase
                            let newUrl = newRuta.snapshot.metadata.downloadURLs[0];

                            //Actualiza los datos del usuario autenticado en este caso se actualiza el
                            //el nombre de la imagen y la url nueva
                            this.store.findRecord("account", IdUsu).then(newData => {
                                //La imagen se guarda con el nombre por viene cargado desde nuestra galeria
                                newData.set('nombreImg', `${file.name}`);
                                newData.set('img', newUrl);


                                newData.save().then(() => {

                                    this.transitionToRoute("logged.ajustes")
                                })
                            });
                        });
                    } else if (nameImageUser != "default") {

                        ///Eliminacion de La imagen Actual y se inserta la nueva imagen seleccionada

                        var urlDeletet = this.get("firebaseApp").storage().ref().child(`imgUsers/${nameImageUser}`);
                        urlDeletet.delete().then(function () {
                            //Aqui la imagen se elimino                        

                        }).catch(function (error) {
                            //Aqui la fallo eliminar imagen
                        });


                        //Crea una ruta o referencia para guardar la imagen en el storage de firebase
                        let newRuta = this.get("firebaseApp").storage().ref().child(`imgUsers/${file.name}`).put(file, metadata);

                        newRuta.on('state_changed', null, (error) => {
                            console.error('Upload Failed:', error);
                        }, () => {

                            //En la variable newUrl se guarda la url de descarga de la imagen de firebase
                            let newUrl = newRuta.snapshot.metadata.downloadURLs[0];

                            //Actualiza los datos del usuario autenticado en este caso se actualiza el
                            //el nombre de la imagen y la url nueva
                            this.store.findRecord("account", IdUsu).then(newData => {
                                //La imagen se guarda con el nombre por viene cargado desde nuestra galeria
                                newData.set('nombreImg', `${file.name}`);
                                newData.set('img', newUrl);



                                newData.save().then(() => {

                                    swal(
                                        'Felicidades!',
                                        'Su imagen se cambio con exito!',
                                        'success'
                                    )
                                })
                            });
                        });
                    }
                })
            }else{
                swal({
                    type: 'error',
                    title: 'Aún no has seleccionado una imagen',
                    text: 'Vuelve a intentarlo',
                    footer: ''
                  })
            }
        },
    }
});
