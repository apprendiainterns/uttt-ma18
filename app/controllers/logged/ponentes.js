import Controller from '@ember/controller';

export default Controller.extend({

    actions: {

        perfilPonente(ponente) {

            this.transitionToRoute("logged.detalleponentes", ponente)
            
        }
    }

});
