import Controller from '@ember/controller';

export default Controller.extend({

    actions: {

        atras(idContacto, idChat) {
            
            this.transitionToRoute("logged.chatU", idContacto, idChat)

        }

    }
});
