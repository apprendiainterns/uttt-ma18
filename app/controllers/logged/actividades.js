import Controller from '@ember/controller';


export default Controller.extend({
 
    actions: {

        //Accion para acceder a la ruta detalle de actividades en donde se consultara
        //la informacion de la actividad seleccionada
        detalle(actividad) {

            //Mandamos el id de la actividad seleccionada por url para posteriormente hacer la consulta detalla de la actividad
            this.transitionToRoute("logged.detalleactividades", actividad)
        },

        ejmplo(destination){

            this.transitionToRoute('logged.actividades', destination)

        },
    }

});
