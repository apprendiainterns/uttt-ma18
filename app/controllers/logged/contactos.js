import Controller from '@ember/controller';
import FindQuery from 'ember-emberfire-find-query/mixins/find-query';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Controller.extend(FindQuery, {
    firebaseApp: service(),

    /* accountsForChat: computed('model.token.eventos.accounts.[]', function () {
        // Regresar el arreglo de account filtrado, todos menos yo
        return console.log(`${this.get('model.account.name')}`)

    }), */

    accountsForChat: computed(function () {
        // Regresar el arreglo de account filtrado, todos menos yo 
        console.log(`${this.get('model.token.account.email')}`)     
        console.log(`${this.get('model.token.evento.name')}`)       
        return console.log(`${this.get('model.token.account.name')}`)

    }),


    actions: {

        chatt(destination, idEvento) {

            var event = idEvento
            let uid = this.get('session.currentUser.uid')
            var resultado = true;
            var valor = true;
            var valorDos = true;

            //Obtengo el id de la cuenta del usuario autenticado por medio de uid
            this.store.query('account', {

                orderBy: "uid",
                equalTo: uid

            }).then((cuentaOrigen) => {

                cuentaOrigen.forEach(element => {

                    //obtengo el id de mi usuario autenticado
                    let idCuentaUser = element.id;

                    //Validar si el usuario autenticado es diferente al contacto para que puedan ejecutarse los chats
                    if (destination != idCuentaUser) {

                        //Buscar cuenta por Id del usuario autenticado
                        this.store.findRecord('account', idCuentaUser).then((myAccount) => {

                            //Obtengo los chats del usuario autenticado
                            myAccount.get("chat").then((chats) => {

                                if (chats.firstObject) {
                                    //Si el usuario tiene algun chat verificar si ese chat coincide con el contacto seleccionado                                        
                                    //Si no coincide algun chat, crear Chat con ese contacto

                                    //Buscar cuenta por Id del contacto seleccionado
                                    this.store.findRecord('account', destination).then((accountDestino) => {

                                        //Obtengo los chats del contacto seleccionado
                                        accountDestino.get("chat").then((chatDestino) => {

                                            //despues consulto si el contacto seleccionado tiene chats... 
                                            //si no tiene ningun chat pero yo si, vamos a crear un nuevo chat relacionado conmigo entramos al else
                                            if (chatDestino.firstObject) {

                                                //si el contacto tiene algun chat hay que buscar el que coincida
                                                //con el chat del usuario Autenticado

                                                //Recorro todos los chats del usuario autenticado
                                                chats.forEach(userChats => {

                                                    //Recorro todos los chats del usuario contacto
                                                    chatDestino.forEach(destinoChats => {

                                                        //comparo ambos chats
                                                        if (userChats.id == destinoChats.id) {
                                                            resultado = false;

                                                            //obtengo los chats del evento
                                                            this.store.query('chat', {
                                                                orderBy: "event",
                                                                equalTo: event

                                                            }).then((chatsEvento) => {

                                                                if (chatsEvento.firstObject) {

                                                                    if (valorDos) {
                                                                        //Recorro todos los chats de mi evento
                                                                        chatsEvento.forEach(element => {

                                                                            let idChatsEvento = element.id;
                                                                            //comparo los chats en comun de los usuarios con los del evento
                                                                            if (userChats.id == idChatsEvento) {
                                                                                valor = false;
                                                                                console.log("6")
                                                                                this.transitionToRoute('logged.chatU', destination, userChats.id)
                                                                                valorDos = false;
                                                                            }
                                                                        })

                                                                        //Convertir estas sentnecia a promesa... Asi como esta funciona con el if
                                                                        if (valor) {
                                                                            //si no se compara el chat entre los usuarios con algun chat del evento crearemos 1
                                                                            this.store.findRecord('evento', event).then((newevento) => {

                                                                                //se compara 2 veces el if(valor) por que se ejecuta el primer if antes del forEach con valor positivo
                                                                                //En el caso cuando se ejecute el for each, el valor es falso por esta razon se vuelve a comparar el if

                                                                                if (valor) {
                                                                                    let chat = this.store.createRecord('chat', {
                                                                                    })
                                                                                    accountDestino.get('chat').pushObject(chat)
                                                                                    myAccount.get('chat').pushObject(chat)
                                                                                    newevento.get('chat').pushObject(chat)

                                                                                    chat.save().then(() => {
                                                                                        accountDestino.save().then(() => {
                                                                                            myAccount.save().then(() => {
                                                                                                newevento.save().then(() => {

                                                                                                    console.log("entra 5")
                                                                                                    valor = false;
                                                                                                    this.transitionToRoute('logged.chatU', destination, chat.id)
                                                                                                })
                                                                                            })
                                                                                        })
                                                                                    })
                                                                                }
                                                                            })
                                                                        }
                                                                    }
                                                                } else {
                                                                    // si el evento no tiene chats se creara 1 chat nuevo por lo tanto
                                                                    //solo entrara la ejecucion de codigo a este else una vez

                                                                    this.store.findRecord('evento', event).then((newevento) => {

                                                                        let chat = this.store.createRecord('chat', {
                                                                        })

                                                                        accountDestino.get('chat').pushObject(chat)
                                                                        myAccount.get('chat').pushObject(chat)
                                                                        newevento.get('chat').pushObject(chat)

                                                                        chat.save().then(() => {
                                                                            accountDestino.save().then(() => {
                                                                                myAccount.save().then(() => {
                                                                                    newevento.save().then(() => {

                                                                                        console.log("entra 4")
                                                                                        valor = false;
                                                                                        this.transitionToRoute('logged.chatU', destination, chat.id)

                                                                                    })
                                                                                })
                                                                            })
                                                                        })
                                                                    })
                                                                }
                                                            })
                                                        }//else
                                                    })
                                                })

                                                if (resultado) {
                                                    //Entará aqui en caso que ningun chat de contacto haya coincidido 
                                                    //con el chat del usuario autenticado y crea uno entre ellos

                                                    this.store.findRecord('account', destination).then((cuentaDestino) => {
                                                        this.store.findRecord('evento', event).then((newevento) => {

                                                            let chat = this.store.createRecord('chat', {
                                                            })

                                                            cuentaDestino.get('chat').pushObject(chat)
                                                            myAccount.get('chat').pushObject(chat)
                                                            newevento.get('chat').pushObject(chat)

                                                            chat.save().then(() => {
                                                                cuentaDestino.save().then(() => {
                                                                    myAccount.save().then(() => {
                                                                        newevento.save().then(() => {

                                                                            console.log("entra 3")
                                                                            resultado = false;
                                                                            this.transitionToRoute('logged.chatU', destination, chat.id)

                                                                        })
                                                                    })
                                                                })
                                                            })
                                                        })
                                                    })
                                                }
                                            } else {

                                                //crearemos el chat con el usuario autenticado y con el usuario destino
                                                //por que el destino no tuvo un chat en el nodo de firebase

                                                this.store.findRecord('account', destination).then((cuentaDestino) => {
                                                    this.store.findRecord('evento', event).then((newevento) => {

                                                        let chat = this.store.createRecord('chat', {
                                                        })

                                                        cuentaDestino.get('chat').pushObject(chat)
                                                        myAccount.get('chat').pushObject(chat)
                                                        newevento.get('chat').pushObject(chat)

                                                        chat.save().then(() => {
                                                            cuentaDestino.save().then(() => {
                                                                myAccount.save().then(() => {
                                                                    newevento.save().then(() => {

                                                                        resultado = false;
                                                                        console.log("entra 2")
                                                                        this.transitionToRoute('logged.chatU', destination, chat.id)

                                                                    })
                                                                })
                                                            })
                                                        })
                                                    })
                                                })
                                            }
                                        })
                                    })
                                } else {

                                    //Crear Chat Si no existe Ninguno relacionado a la cuenta
                                    //Es la Primera vez que accede a un chat cualquiera

                                    this.store.findRecord('account', destination).then((cuentaDestino) => {
                                        this.store.findRecord('evento', event).then((newevento) => {

                                            let chat = this.store.createRecord('chat', {
                                            })

                                            cuentaDestino.get('chat').pushObject(chat)
                                            myAccount.get('chat').pushObject(chat)
                                            newevento.get('chat').pushObject(chat)

                                            chat.save().then(() => {
                                                cuentaDestino.save().then(() => {
                                                    myAccount.save().then(() => {
                                                        newevento.save().then(() => {

                                                            console.log("entra 1")
                                                            resultado = false;
                                                            this.transitionToRoute('logged.chatU', destination, chat.id)

                                                        })
                                                    })
                                                })
                                            })
                                        })
                                    })
                                }
                            })
                        })
                    } else {
                        alert("Seleccione un usuario diferente al de usted")
                    }
                })
            })
        },

        ejmplo(deto, deto2){
            this.transitionToRoute('logged.chatU', deto, deto2)
        },

        gotoChat(){
            this.transitionToRoute('logged.chatU', destination, chat)
        },

    }
});
