import Controller from '@ember/controller';

export default Controller.extend({

    actions: {

        perfil(idContacto, idChat) {

            this.transitionToRoute("logged.perfilcontacto", idContacto, idChat)

        }

    }

});
