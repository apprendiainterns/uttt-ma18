import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({
    queryParams: ['source'],
    source: 'logged.actividades',

    datos: computed(function () {

        // Regresar el arreglo de account filtrado, todos menos yo 
        console.log(`${this.get('model.schedule')}`)

    }),

    actions:{

        recordar(){

         /*    swal({
                title: '¿Dejar de recordar la actividad?',
                text: '',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }) */

            swal(
                '',
                'Actividad agregada al recordatorio',
                'success'
            )

        }

    }
});
