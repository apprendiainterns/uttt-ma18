import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { validator, buildValidations } from 'ember-cp-validations';

const Validations = buildValidations({

    //Validacion para input token
    
    'token': [
        //Validacion texto esta vacio
        validator('presence', {
            presence: true,
            message: 'El campo no puede estar vacio'
        }),
        //Validacion texto de solo 6 caracteres
        validator('length', {
            max: 6,
            min: 6,
            message: 'Debe ingresar 6 digitos'
        })

    ],
});

export default Controller.extend(Validations, {
    session: service(),
    currentEvent: service(),

    actions: {

        //Muestra el error del token de acuerdo a las cp-validations
        toogleError(attr) {
            switch (attr) {
                case 'token':
                    this.set('tokenErrorCheck', true)
                    break
            }
        },

        //Action para entrar a todo el contenido del evento con el repestivo token
        login(token) {
            
            this.validate().then(({ validations }) => {

                //esto es para que tome en cuenta las validaciones del token al dar click en el boton
                //y si nada falla se entre al contenido de la aplicacion

                if (validations.get('isValid')) {

                    //buscamos el token agregado con los tokens en base de datos
                    this.store.query('token', {

                        orderBy: "codigo_token",
                        equalTo: token

                    }).then((codigo) => {

                        
                        if (!codigo.firstObject) {
                            //si no existe el token manda una alerta (sweetAlert)
                            
                            swal({
                                type: 'error',
                                title: 'Algo salió mal...',
                                text: 'No existe el token, vuelve a intentarlo',
                                footer: ''
                              })

                            //Pone el input de token en blanco
                            this.set('token', '')

                        } else {

                            //Si el token existe lo recorremos con el forEach para obtener sus datos especificos de todo el objeto
                            codigo.forEach(newtoken => {
                                
                                
                                    //obtenemos el account relacionado con el token ingresado
                                    newtoken.get('account').then((cuentaToken) => {

                                        let pass = cuentaToken.pass;
                                        let correo = cuentaToken.email
                                        
                                        //Obtenemos el evento relaciona con el token ingresado
                                        newtoken.get('evento').then(() => {

                                            //validaciones para autenticacion a firebase cuenta con los datos de la cuenta relaccionada con el token
                                            if (correo != null) {
                                               
                                                if (correo.search('@') == -1) {
                                                    alert('No es un correo')
                                                }
                                                //Authenticacion a firebase
                                                this.get('session').open('firebase', {
                                                    provider: 'password',
                                                    email: correo,
                                                    password: pass
                                                }).then(() => {
                                                    //si
                                                    this.set('token', '')
                                                    //Si todo es correcto, eccedemos a el cntenido de la aplicacion y manadamos el id del token por la url
                                                    this.transitionToRoute('logged.home', newtoken.get('id'))
                                                }).catch((e) => {
                                                    //console(e)

                                                    //Pasa esto en caso de un error                                                    
                                                    switch (e.code) {
                                                        case "auth/user-not-found":
                                                            alert('usuario no encontrado')
                                                            break;
                                                        case "auth/wrong-password":
                                                            alert('contraseña incorrecta')
                                                    }
                                                })
                                            }else{

                                                //entra aqui si la cuenta relacionada con el evento no tiene correo
                                                alert('No existe un correo valido para este token')
                                        
                                                this.set('token', '')
                                            }
                                        });
                                    })                                
                            })
                        }
                    }).catch((e) => {
                        console.log(e)
                    });
                }
            })
        }
    }
});
