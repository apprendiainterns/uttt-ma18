import Route from '@ember/routing/route';
import RSVP from 'rsvp';
import DS from 'ember-data'
import FindQuery from 'ember-emberfire-find-query/mixins/find-query';

export default Route.extend(FindQuery,{

    model(params) {

        return RSVP.hash({

            cargarUsuarios: this.store.find('account', params.c_id),

            cargarUsuarioaa: this.store.query('account', {

                orderBy: 'id',
                equalTo: params.c_id,                
                             
            }),

            cargarUsuario: DS.PromiseArray.create({
                promise: new Promise((resolve)=>{
                    
                    this.filterEqual(this.store, 'account', { 'id': params.c_id}, function (toms) {
                        console.log(toms)
                        return resolve(toms)                                                                               
                    })
                })
            })      


        })

    }

});
