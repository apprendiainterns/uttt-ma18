import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import RSVP from 'rsvp';
import DS from 'ember-data'

export default Route.extend({
	ebentoApi: service(),

	queryParams: {
		source: {
			refreshModel: true
		}
	},

	refreshModel: true,


	model(params) {		
		
		return RSVP.hash({

			firstDay: this.get('ebentoApi').initialize('zcP6jVFpr8WavZc8LvKucwDysLBQUNdS4t2kaMFGTDu47Xrc', '5aa1807098759fc51bf9ea43').then(() => {
				return this.get('ebentoApi').request('schedule').then((data) => {

					let diaUno = data.firstObject;									
					return diaUno;
				})
			}),
		
			schedule: DS.PromiseArray.create({
				promise: new Promise((resolve) => {

					this.get('ebentoApi').initialize('zcP6jVFpr8WavZc8LvKucwDysLBQUNdS4t2kaMFGTDu47Xrc', '5aa1807098759fc51bf9ea43').then(() => {
						return this.get('ebentoApi').request('schedule').then((data) => {							

							return resolve(data);
/* 

							if (params.actividad != "act") {

								var filtro = data.find(act => act.id == params.actividad)

								return resolve(filtro);

							} else {

								var filtro = data.firstObject;

								console.log(filtro)

								return resolve(filtro);

							} */
						})
					})
				})
			}),

			fecha: params.actividad
		})
	}

});
