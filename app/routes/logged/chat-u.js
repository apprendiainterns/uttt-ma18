import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import RSVP from 'rsvp';
import FindQuery from 'ember-emberfire-find-query/mixins/find-query';
import DS from 'ember-data'

export default Route.extend(FindQuery,{

    session: service(),
    infinity: service(),

    model(params){

        let uid = this.get('session.currentUser.uid')
        
        return RSVP.hash({
 
            cargarDatos: this.store.find('chat', params.c_user),
            cargarNombre: this.store.find('account', params.c_id),     
            
            //cargarChat: this.infinity.model('chat'),

            cargarChat: DS.PromiseArray.create({
                promise: new Promise((resolve)=>{
                    
                    this.filterEqual(this.store, 'chat', { 'id': params.c_user}, function (toms) {                        
                        return resolve(toms)                                                                               
                    })
                })
            }),

            cargarDatosUsuario: this.store.query('account', {
				orderBy: "uid",
				equalTo: uid
			})
        })

    }
});
