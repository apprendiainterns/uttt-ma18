import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
	ebentoApi: service(),

	model() {

		/* return this.get('ebentoApi').initialize('zcP6jVFpr8WavZc8LvKucwDysLBQUNdS4t2kaMFGTDu47Xrc', '5aa1807098759fc51bf9ea43').then(() => {
			return this.get('ebentoApi').request('speakers').then((data) => {				
				return data;
			})
		}) */

		return this.get('ebentoApi').initialize('ercFmYtUNUKdn1JDPb-URH_JNoTZZ6FKcKQgouYUfmxQaKXa', '5ac51c8c98759fcb4a8a4bbf').then(() => {
			return this.get('ebentoApi').request('speakers').then((data) => {				
				return data;
			})
		})

	}
});
