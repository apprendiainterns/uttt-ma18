import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import RSVP from 'rsvp';

export default Route.extend({
  ebentoApi: service(),


  queryParams: {
    source: {
      refreshModel: true
    }
  },

  model(params) {

    return RSVP.hash({


      actividad: params.actividad,


      schedule: this.get('ebentoApi').initialize('zcP6jVFpr8WavZc8LvKucwDysLBQUNdS4t2kaMFGTDu47Xrc', '5aa1807098759fc51bf9ea43').then(() => {
        return this.get('ebentoApi').request('schedule').then((data) => {

          return data;

        })
      })


    })

  }
});
