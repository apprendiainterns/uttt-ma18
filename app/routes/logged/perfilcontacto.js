import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({

    model(params){

        return RSVP.hash({

        contacto: this.store.find('account', params.contact),
        chat: this.store.find('chat', params.chat),

        })

    }
});
