import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
	session: service(),
	//Mantener la session del usuario iniciada durante el proceso

	beforeModel() {
		return this.get('session').fetch().then(this.checkAuth.bind(this)).catch(this.checkAuth.bind(this))
	},
	checkAuth() {

		//Si el usuario ya esta authenticado accede a la vista looged/home
		if (this.get('session.isAuthenticated')) {
			return this.transitionTo('logged.home') 
		}
}
});