import DS from 'ember-data';

export default DS.Model.extend({

    codigo_token: DS.attr('string'),    
    evento: DS.belongsTo('evento'),
    account: DS.belongsTo('account'),

});
