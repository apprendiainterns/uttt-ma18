import DS from 'ember-data';

export default DS.Model.extend({

    message: DS.attr('string'),
    hora: DS.attr('string'),
    fecha: DS.attr('string'),
    estado: DS.attr('string'),
    sender: DS.belongsTo('account'),
    chat: DS.belongsTo('chat'),

});
