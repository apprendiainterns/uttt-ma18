'use strict';

module.exports = function (environment) {
  let ENV = {
    modulePrefix: 'ebentoapp',
    environment,
    rootURL: '',
    locationType: 'hash',
    torii: {
      sessionServiceName: 'session'
    },
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
    ENV['firebase'] = {
      // TUDU: add configuration to development db 
      apiKey: "AIzaSyDRD8Gdu0vtpc2SzbVpjrbs1Vy_IyB-QbU",
    authDomain: "ebento-26f43.firebaseapp.com",
    databaseURL: "https://ebento-26f43.firebaseio.com",
    projectId: "ebento-26f43",
    storageBucket: "ebento-26f43.appspot.com",
    messagingSenderId: "715591699569"
    }
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
  }

  if (environment === 'production') {
    // here you can enable a production-specific feature
    ENV['firebase'] = {
      // TUDU: add configuration to production db 
    }
  }

  return ENV;
};
